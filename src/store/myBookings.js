import axios from "axios";

const state={
    mybookings:[]
};
const getters={
    getMyBooking: (state) => state.mybookings}
const mutations={
    setMyBooking(state, payload) {
        state.mybookings=payload;
      },
};
const actions={
    bookAction({ commit }, payload) {
        return new Promise((resolve, reject) => {
          axios.post("https://612fc48a5fc50700175f1748.mockapi.io/bookings", payload).then(
            (response) => {
              resolve(response);
            }).catch(
            (error) => {
                alert(error)
              reject(error);
            })
        });
      },
      getBookingAction({ commit }) {
        return new Promise((resolve, reject) => {
          axios.get("https://api.football-data.org/v2/competitions").then(
            (response) => {
                commit("setMyBooking", response.data.competitions);
              resolve(response);
            }).catch(
            (error) => {
              reject(error);
            })
        });
      },
        deleteMyBookingAction({ commit }, payload) {
          return new Promise((resolve, reject) => {
            axios.post("https://612fc48a5fc50700175f1748.mockapi.io/bookings", payload).then(
              (response) => {
                alert("Ticket cancelled successfully !!")
                resolve(response);
              }).catch(
              (error) => {
                  alert(error)
                reject(error);
              })
          });
        },
};
   
export default{
    state,
    mutations,
    actions,
    getters
}