import Vue from 'vue'
import Vuex from 'vuex'

import schedule from './schedule'
import myBookings from './myBookings'


Vue.use(Vuex)

export default new Vuex.Store({
  modules:
  {
    schedule,
    myBookings,
  }
})
